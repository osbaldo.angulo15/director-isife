// @flow
import React from 'react';
import {component} from 'rrsx';
import {Container, GlobalTypography} from './styled';
import {Grid, Button, Typography} from '@material-ui/core';
import CircularProgressBar from '../circularProgressBar';

type Props = {
    children: any,
    style: CSSStyleDeclaration,
    className: string,
    titulo: String,
    conseguido: Number,
    total: Number,
    tipo: Number,
    handleVerDetalle: ()=>{},
    porcentaje: Number,
};

const RegistroCard = ({style, children, className, titulo, conseguido, total, tipo, handleVerDetalle, porcentaje}: Props) => {
  return <div className={className} style={style}>
    <Container tipo={tipo}>
      {tipo === 1 ? (
      <>
        <Grid container justify="center"><GlobalTypography variant="h5" style={{textAlign: 'center'}}>{titulo}</GlobalTypography></Grid>
        <Grid style={{position: 'relative', left: '50%', marginLeft: '-70px'}}><CircularProgressBar CPBWidth="40%" percentage={porcentaje}/></Grid>
        <Grid container style={{textAlign: 'center'}}><GlobalTypography><strong>{`${conseguido} de ${total} `}</strong>han finalizado satisfactoriamente el registro de información.</GlobalTypography></Grid>
        <Grid container justify="center"><Button color="primary" onClick={()=>handleVerDetalle()}><Typography>VER DETALLE</Typography></Button></Grid>
      </>
        ) : (
        <>
          <Grid container><GlobalTypography variant="h5">{titulo}</GlobalTypography></Grid>
            <Grid item style={{position: 'relative', left: '20%', marginLeft: '-70px'}}><CircularProgressBar CPBWidth="25%" percentage={porcentaje}/></Grid>
            <Grid item style={{position: 'relative', left: '120px', bottom: '110px', width: '230px'}}><GlobalTypography><strong>{`${conseguido} de ${total} `}</strong>han finalizado satisfactoriamente el registro de información.</GlobalTypography></Grid>
        </>
        )}
    </Container>
    {children}
  </div>;
};

export default component<Props>(RegistroCard);
