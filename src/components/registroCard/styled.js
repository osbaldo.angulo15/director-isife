import styled from 'styled-components';
import {Grid, Typography} from '@material-ui/core';

export const Container = styled(Grid)`
    background-color: #FFFFFF;
    border-radius: 2px;
    box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.16);
    width: ${(props) => props.tipo === 1 ? ('317px') : ('386px')};;
    height: ${(props) => props.tipo === 1 ? ('352px') : ('172px')};;
    padding: 1%;
`;

export const GlobalTypography = styled(Typography)`
    color: #424242;
`;
