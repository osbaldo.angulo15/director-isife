import styled from 'styled-components';
import {Typography, Grid, DialogContent, ExpansionPanelSummary} from '@material-ui/core';

export const TituloTypography = styled(Typography)`
    color: #757575;
`;

export const Divisor = styled(Grid)`
    background-color: #979797;
    opacity: 0.24;
    height: 2px;
    width: 100%;
`;

export const Item = styled(Grid)`
    height: 50px;
`;

export const ExpansionPanelTypography = styled(Typography)`
`;

export const ExpansionPanelDivisor = styled(ExpansionPanelSummary)`
    &:hover{
        background-color: #E3F0F8;
        ${ExpansionPanelTypography}{
            color: #219EEC;
        }
    }
`;

export const Contenido = styled(DialogContent)`
    overflow-y: scroll !important;
 ::-webkit-scrollbar {
      -webkit-appearance: none;
  }
  ::-webkit-scrollbar:vertical {
      width: 5px;
  }
  ::-webkit-scrollbar:horizontal {
      height: 12px !important;
  }
  ::-webkit-scrollbar-thumb {
      background-color: #219EEC;
      border-radius: 0px;
      border: 0px solid #ffffff;
  }
  ::-webkit-scrollbar-track {
      border-radius: 0px;
      background-color: transparent;
  }
`;
