// @flow
import React from 'react';
import {component} from 'rrsx';
import {Dialog, DialogTitle, Typography, Grid, IconButton, ExpansionPanel} from '@material-ui/core';
import {Close, ExpandMore} from '@material-ui/icons';
import {TituloTypography, Divisor, Item, Contenido, ExpansionPanelDivisor, ExpansionPanelTypography} from './styledComponentes';

type Props = {
    children: any,
    style: CSSStyleDeclaration,
    className: string,
    titulo: String,
    informacion: Array<any>,
    handleClose: ()=>{},
    open: Boolean,
};

const ModalAltaRegistro = ({style, children, className, titulo, informacion, handleClose, open}: Props) => {
  return <div className={className} style={style}>
    <Dialog fullWidth maxWidth="md" open={open}>
      <DialogTitle>
        <Grid container alignItems="center">
          <Grid item xs={11}>
            <Typography variant="h5"><strong>{titulo}</strong></Typography>
          </Grid>
          <Grid item xs={1}>
            <IconButton onClick={()=>handleClose()}><Close fontSize="large"/></IconButton>
          </Grid>
        </Grid>
      </DialogTitle>
      <Contenido>
        {informacion.map((item, key)=>(
            <>
            {key !== 0 ? (
                <ExpansionPanel key={key} style={{boxShadow: 'none'}}>
                  <ExpansionPanelDivisor
                    expandIcon={<ExpandMore fontSize="large"/>}
                  >
                    {item.map((info, index)=>(
                        <>
                        {info.titulo === 'Cargo' ? (
                        <ExpansionPanelTypography>{`Nivel: ${info.subTitulo}`}</ExpansionPanelTypography>
                        ) : null}
                        </>
                    ))}
                  </ExpansionPanelDivisor>
                  {item.map((info, index)=>(
                    <>
                        <Item container alignItems="center" key={`${info.id}${key}`}>
                          <Grid item xs={6}><TituloTypography variant="subtitle1">{info.titulo}</TituloTypography></Grid>
                          <Grid item xs={6}><Typography variant="subtitle1">{info.subTitulo}</Typography></Grid>
                        </Item>
                        <Divisor/>
                    </>
                  ))}
                </ExpansionPanel>
            ) : (
                <>
                  {item.map((info, index)=>(
                <>
                    <Item container alignItems="center" key={`${info.id}${key}`}>
                      <Grid item xs={6}><TituloTypography variant="subtitle1">{info.titulo}</TituloTypography></Grid>
                      <Grid item xs={6}><Typography variant="subtitle1">{info.subTitulo}</Typography></Grid>
                    </Item>
                    <Divisor/>
                </>
                  ))}
            </>
            )}
          </>
        ))}
      </Contenido>
    </Dialog>
    {children} </div>;
};

export default component<Props>(ModalAltaRegistro);
