import styled from 'styled-components';
import {Grid, Typography} from '@material-ui/core';

export const Container = styled(Grid)`
    background-image: ${(props) => props.activebutton ? ('linear-gradient(41.12deg, #219EEC 0%, #6CC7FF 100%)') : ('linear-gradient(41.12deg, rgba(66, 66, 66, 0.24) 0%, rgba(117, 117, 117, 0.16) 100%)')};
    border-radius: 6px;
    box-shadow: 0 2px 7px 0 rgba(0, 0, 0, 0.1), inset 0 -1px 0 0 rgba(0, 0, 0, 0.08);
    width: 197.6px;
    height: 156px;
    padding: 1%;
    margin-right: 1%;
    cursor: pointer;
`;

export const GlobalTypography = styled(Typography)`
    color: white;
`;
