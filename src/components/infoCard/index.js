// @flow
import React from 'react';
import {component} from 'rrsx';
import {Container, GlobalTypography} from './styled';
import {Grid} from '@material-ui/core';

type Informacion = {
    children: any,
    style: CSSStyleDeclaration,
    className: string,
    id: String,
    porcentaje: Number,
    subTitulo: String,
};

type Props = {
    children: any,
    style: CSSStyleDeclaration,
    className: string,
    activeButton: Boolean,
    informacion: Array<Informacion>,
    handleActive: ()=>{},
};

const InfoCard = ({style, children, className, informacion, handleActive, activeButton}: Props) => {
  return <div className={className} style={style}>
    <Grid container>
      {informacion.map((data, index)=>(
        <Container activebutton={activeButton === index ? true : false} container key={data.id} onClick={()=>handleActive(index)}>
          <Grid container justify="center"><GlobalTypography variant="h4">{`${data.porcentaje}%`}</GlobalTypography></Grid>
          <Grid container style={{textAlign: 'center'}}><GlobalTypography variant="subtitle1">{data.subTitulo}</GlobalTypography></Grid>
        </Container>
      ))}
    </Grid>
    {children} </div>;
};

export default component<Props>(InfoCard);
