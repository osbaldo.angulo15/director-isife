import styled from 'styled-components';
import {Grid, Typography} from '@material-ui/core';

export const Container = styled(Grid)`
    background-color: #FFFFFF;
    border-radius: 2px;
    box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.16);
    width: 324px;
    height: 447px;
    padding: 1%;
`;

export const Divisor = styled(Grid)`
    background-color: #979797;
    opacity: 0.24;
    height: 2px;
    width: 100%;
`;

export const Item = styled(Grid)`
    height: 30px;
`;

export const ItemContainer = styled(Grid)`
    height: 340px;
    overflow-y: scroll !important;
    ::-webkit-scrollbar {
        -webkit-appearance: none;
    }
    ::-webkit-scrollbar:vertical {
        width: 5px;
    }
    ::-webkit-scrollbar:horizontal {
        height: 12px !important;
    }
    ::-webkit-scrollbar-thumb {
        background-color: #219EEC;
        border-radius: 0px;
        border: 0px solid #ffffff;
    }
    ::-webkit-scrollbar-track {
        border-radius: 0px;
        background-color: transparent;
    }
`;

export const GlobalTypography = styled(Typography)`
    color: white;
`;
