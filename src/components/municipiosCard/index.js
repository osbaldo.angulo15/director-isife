// @flow
import React from 'react';
import {component} from 'rrsx';
import {Container, Divisor, Item, ItemContainer} from './styledComponents';
import {Grid, Typography} from '@material-ui/core';

type municipios = { id: String, municipio: String, aulas: Number };
type Props = { children: any, style: CSSStyleDeclaration, className: string, informacion: Array<municipios>, tipo: String };

const MunicipiosCard = ({style, children, className, informacion, tipo}: Props) => {
  return <div className={className} style={style}>
    <Container container>
      <Grid container>
        <Grid container item xs={6} justify="center"><Typography variant="subtitle1">Municipio</Typography></Grid>
        <Grid container item xs={6} justify="center"><Typography variant="subtitle1">{tipo}</Typography></Grid>
        <Divisor/>
      </Grid>
      <ItemContainer container>
        {informacion.map((municipio)=>(
          <Item container key={municipio.id}>
            <Grid container item xs={6} justify="center"><Typography variant="subtitle1">{municipio.municipio}</Typography></Grid>
            <Grid container item xs={6} justify="center"><Typography variant="subtitle1">{municipio.aulas}</Typography></Grid>
          </Item>
        ))}
      </ItemContainer>
    </Container>
    {children} </div>;
};

export default component<Props>(MunicipiosCard);
