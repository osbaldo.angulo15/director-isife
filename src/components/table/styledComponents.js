import styled from 'styled-components';
import {Grid} from '@material-ui/core';

export const Container = styled(Grid)`
    background-color: #FFFFFF;
    border-radius: 2px;
    box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.16);
    width: 100%;
    height: auto;
    padding: 1%;
`;

export const Divisor = styled(Grid)`
    background-color: #979797;
    opacity: 0.24;
    height: 1px;
    width: 100%;
`;

export const Header = styled(Grid)`
    height: 68px;
`;

export const ItemContainer = styled(Grid)`
    height: 52px;
`;
