// @flow
import React from 'react';
import {component} from 'rrsx';
import {Container, Header, ItemContainer, Divisor} from './styledComponents';
import {Typography, Grid, IconButton} from '@material-ui/core';
import {ErrorOutline} from '@material-ui/icons';


type informacion = { id: String, };
type Props = { children: any, style: CSSStyleDeclaration, className: string, titulo: String, informacion: Array<informacion>, tipo: Number, handleOpen: ()=>{}};

const Table = ({style, children, className, titulo, informacion, tipo, handleOpen}: Props) => {
  return <div className={className} style={style}>
    <Container container>
      <Header container alignItems="center"><Typography variant="h5"><strong>{titulo}</strong></Typography></Header>
      {informacion.map((row)=>(
        <ItemContainer container key={row.id}>
          <Grid container item xs={tipo === 1 ? 6 : 5} alignItems="center"><Typography variant="subtitle1" style={{color: '#757575'}}>{row.titulo}</Typography></Grid>
          <Grid container item xs={6} alignItems="center"><Typography variant="subtitle1">{row.subTitulo}</Typography></Grid>
          {tipo !== 1 ? (<Grid item xs={1}><IconButton color="primary" onClick={()=>handleOpen()}><ErrorOutline /></IconButton></Grid>) : null}
          <Divisor/>
        </ItemContainer>
      ))}
    </Container>
    {children} </div>;
};

export default component<Props>(Table);
