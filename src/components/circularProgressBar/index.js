// @flow
import React from 'react';
import {component} from 'rrsx';
import {CircularProgressbarWithChildren, buildStyles} from 'react-circular-progressbar';
import {Grid, Typography} from '@material-ui/core';

type Props = { children: any, style: CSSStyleDeclaration, className: string, percentage: Number, description: String, label: String, CPBWidth: String};

const CircularPogressBar = ({style, children, className, percentage, CPBWidth}: Props) => {
  function Example(props) {
    return (
      <div>
        <div style={{width: `${CPBWidth}`}}>{props.children}</div>
        <h3 className="h5">{props.label}</h3>
      </div>
    );
  }
  return <div className={className} style={style}>
    <Example>
      <CircularProgressbarWithChildren
        value={percentage}
        styles={buildStyles({
          fill: '#1D9DEC',
          fontSize: '30px',
          pathTransitionDuration: 1.5,
          pathColor: `rgba(29, 157, 236, ${percentage / 100})`,
          trailColor: '#d6d6d6',
          strokeLinecap: 'round',
        })}
      >
        <Grid container justify="center">
          <Typography style={{color: '#1D9DEC'}} variant="h6"><strong>{`${percentage}%`}</strong></Typography>
        </Grid>
        <Grid container justify="center">
          <Typography style={{color: '#1D9DEC'}} variant="subtitle1"><strong>del total</strong></Typography>
        </Grid>
      </CircularProgressbarWithChildren>
    </Example>
    {children} </div>;
};

export default component<Props>(CircularPogressBar);
