import Blue from '@material-ui/core/colors/blue';

export const GlobalTheme = {
  typography: {
    fontFamily: ['"Open Sans"', 'sans-serif'].join(','),
  },
  palette: {
    primary: Blue,
  },
};
