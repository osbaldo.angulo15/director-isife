export const CARDS = [
  {
    id: 1,
    porcentaje: 91.02,
    subTitulo: 'Escuelas sin aulas de carton y con todos los servicios básicos.',
  },
  {
    id: 2,
    porcentaje: 100,
    subTitulo: 'Escuelas sin aulas de carton y con todos los servicios básicos.',
  },
  {
    id: 3,
    porcentaje: 87,
    subTitulo: 'Escuelas sin aulas de carton y con todos los servicios básicos.',
  },
]
;

export const INFORMACION = [
  {
    id: 1,
    titulo: 'Nombre',
    subTitulo: 'Ramón López Velarde',
  },
  {
    id: 2,
    titulo: 'CCT',
    subTitulo: '25EPR0001D',
  },
  {
    id: 5,
    titulo: 'Zona escolar',
    subTitulo: '30',
  },
  {
    id: 4,
    titulo: 'Sector',
    subTitulo: '02',
  },
  {
    id: 5,
    titulo: 'Subsistema',
    subTitulo: 'Estatal',
  },
  {
    id: 6,
    titulo: 'C.P.',
    subTitulo: '80200',
  },
  {
    id: 7,
    titulo: 'Direccion',
    subTitulo: 'Jorge Almada, 80200 Culiacán Rosales, Sin.',
  },
]
;

export const MUNICIPIOS = [
  {
    id: 1,
    aulas: 91.02,
    municipio: 'Guasave',
  },
  {
    id: 2,
    aulas: 100,
    municipio: 'Guasave',
  },
  {
    id: 3,
    aulas: 87,
    municipio: 'Guasave',
  },
  {
    id: 1,
    aulas: 91.02,
    municipio: 'Guasave',
  },
  {
    id: 2,
    aulas: 100,
    municipio: 'Guasave',
  },
  {
    id: 3,
    aulas: 87,
    municipio: 'Guasave',
  },
  {
    id: 1,
    aulas: 91.02,
    municipio: 'Guasave',
  },
  {
    id: 2,
    aulas: 100,
    municipio: 'Guasave',
  },
  {
    id: 3,
    aulas: 87,
    municipio: 'Guasave',
  },
  {
    id: 1,
    aulas: 91.02,
    municipio: 'Guasave',
  },
  {
    id: 2,
    aulas: 100,
    municipio: 'Guasave',
  },
  {
    id: 3,
    aulas: 87,
    municipio: 'Guasave',
  },
  {
    id: 1,
    aulas: 91.02,
    municipio: 'Guasave',
  },
  {
    id: 2,
    aulas: 100,
    municipio: 'Guasave',
  },
  {
    id: 3,
    aulas: 87,
    municipio: 'Guasave',
  },
  {
    id: 1,
    aulas: 91.02,
    municipio: 'Guasave',
  },
  {
    id: 2,
    aulas: 100,
    municipio: 'Guasave',
  },
  {
    id: 3,
    aulas: 87,
    municipio: 'Guasave',
  },
  {
    id: 1,
    aulas: 91.02,
    municipio: 'Guasave',
  },
  {
    id: 2,
    aulas: 100,
    municipio: 'Guasave',
  },
  {
    id: 3,
    aulas: 87,
    municipio: 'Guasave',
  },
  {
    id: 1,
    aulas: 91.02,
    municipio: 'Guasave',
  },
  {
    id: 2,
    aulas: 100,
    municipio: 'Guasave',
  },
  {
    id: 3,
    aulas: 87,
    municipio: 'Guasave',
  },
]
;

export const RESPONSABILIDAD =
[
  [
    {
      id: 1,
      titulo: 'Nombre',
      subTitulo: 'Emiliano Cepeda Armenta',
    },
    {
      id: 2,
      titulo: 'CURP',
      subTitulo: 'CEAE790910HSLLNR01',
    },
    {
      id: 3,
      titulo: 'Clave de elector',
      subTitulo: 'CEAE790910H100',
    },
    {
      id: 4,
      titulo: 'Correo electrónico',
      subTitulo: 'CEAE790910HSLLNR01@sepyc.edu.mx',
    },
    {
      id: 5,
      titulo: 'Número de celular',
      subTitulo: '6671231203',
    },
    {
      id: 6,
      titulo: 'Cargo',
      subTitulo: 'Supervisor de Zona Escolar 030',
    },
    {
      id: 7,
      titulo: 'Domicilio',
      subTitulo: 'Calle cerrala del valle #56, Colonia bosques II etapa, Culiacán, Sinaloa.',
    },
    {
      id: 8,
      titulo: 'Corresponsabilidad de registro y alta de usuario',
      subTitulo: 'José Ángel Santiesteban Medina',
    },
  ],
  [
    {
      id: 1,
      titulo: 'Nombre',
      subTitulo: 'José Ángel Santiesteban Medina',
    },
    {
      id: 2,
      titulo: 'CURP',
      subTitulo: 'SAMJJ890121HSLLNS04',
    },
    {
      id: 3,
      titulo: 'Clave de elector',
      subTitulo: 'SAMJJ890121H100',
    },
    {
      id: 4,
      titulo: 'Correo electrónico',
      subTitulo: 'SAMJJ890121HSLLNS04@sepyc.edu.mx',
    },
    {
      id: 5,
      titulo: 'Número de celular',
      subTitulo: '6671231203',
    },
    {
      id: 6,
      titulo: 'Cargo',
      subTitulo: 'Jefatura de Sector 02',
    },
    {
      id: 7,
      titulo: 'Domicilio',
      subTitulo: 'Calle cerrala del valle #56, Colonia bosques II etapa, Culiacán, Sinaloa.',
    },
    {
      id: 8,
      titulo: 'Corresponsabilidad de registro y alta de usuario',
      subTitulo: 'María Esthela Cervantes Soto',
    },
  ],
  [
    {
      id: 1,
      titulo: 'Nombre',
      subTitulo: 'Maria Esthela Cervantes Soto',
    },
    {
      id: 2,
      titulo: 'CURP',
      subTitulo: 'CESM710921MSLLN00',
    },
    {
      id: 3,
      titulo: 'Clave de elector',
      subTitulo: 'CESM710921M200',
    },
    {
      id: 4,
      titulo: 'Correo electrónico',
      subTitulo: 'CESM710921MSLLN00@sepyc.edu.mx',
    },
    {
      id: 5,
      titulo: 'Número de celular',
      subTitulo: '6671239012',
    },
    {
      id: 6,
      titulo: 'Cargo',
      subTitulo: 'Jefe de Servicios Regionales',
    },
    {
      id: 7,
      titulo: 'Domicilio',
      subTitulo: 'Calle Sindicalismo #1831, Colonia Valle del Real, Culiacán, Sinaloa.',
    },
    {
      id: 8,
      titulo: 'Corresponsabilidad de registro y alta de usuario',
      subTitulo: 'Juan Manuel Osuna Lizarraga',
    },
  ],
  [
    {
      id: 1,
      titulo: 'Nombre',
      subTitulo: 'Juan Manuel Osuna Lizarraga',
    },
    {
      id: 2,
      titulo: 'CURP',
      subTitulo: 'OSLJ780203HSLLNR09',
    },
    {
      id: 3,
      titulo: 'Clave de elector',
      subTitulo: 'OSLJ780203H100',
    },
    {
      id: 4,
      titulo: 'Correo electrónico',
      subTitulo: 'OSLJ780203HSLLNR09@sepyc.edu.mx',
    },
    {
      id: 5,
      titulo: 'Número de celular',
      subTitulo: '6679901200',
    },
    {
      id: 6,
      titulo: 'Cargo',
      subTitulo: 'Director de Servicios Regionales de SEPyC',
    },
    {
      id: 7,
      titulo: 'Domicilio',
      subTitulo: 'Blvd. Pedro Infante 2200 Poniente, Recursos Hidráulicos, Culiacán, Sin.',
    },
  ],
];
