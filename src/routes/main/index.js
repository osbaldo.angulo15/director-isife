// @flow
import React from 'react';
import {component} from 'rrsx';
import RegistroCard from '../../components/registroCard';

type Props = { children: any, style: CSSStyleDeclaration, className: string };

const componentName = ({style, children, className}: Props) => {
  return <div className={className} style={style}> <RegistroCard titulo="Cobertura global" tipo={2} porcentaje={50} conseguido={50} total={100}/> {children} </div>;
};

export default component<Props>(componentName);
